module gitlab.com/m242/html2text-aercfilter

go 1.15

require (
	github.com/olekukonko/tablewriter v0.0.4 // indirect
	github.com/ssor/bom v0.0.0-20170718123548-6386211fdfcf // indirect
	golang.org/x/net v0.0.0-20201201195509-5d6afe98e0b7 // indirect
	jaytaylor.com/html2text v0.0.0-20200412013138-3577fbdbcff7
)
