package main

import (
	"bufio"
	"fmt"
	"jaytaylor.com/html2text"
	"os"
)

func main() {
	r := bufio.NewReader(os.Stdin)
	text, err := html2text.FromReader(r, html2text.Options{PrettyTables: true})
	if err != nil {
		panic(err)
	}
	fmt.Println(text)
}
